﻿using UnityEngine;

public interface IBrainCollisionLogic
{
    void OnCollision(Entity entity,Collision2D collision);
    void OnCollisionStopped(Entity entity,Collision2D collision);
}
