﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName =  nameof(AttackOnHit), menuName = "Brains/Fighting Logic/" + nameof(AttackOnHit))]
public class AttackOnHit : BrainLogic,IBrainCollisionLogic
{
    public float coolDown;
    private bool isHittingTarget;
    private DateTime lastAttackTime;
    
    public override void Think(Entity entity)
    {
        Brain brain = entity.Brain;
        // TODO this is really retarded, melee damage should be part of weapons system
        if (isHittingTarget && lastAttackTime.HasSecondsPassed(coolDown))
        {
            Damagable damagable = brain.Target.GetComponent<Damagable>();
            if (damagable != null)
            {
                damagable.TakeDamage(new FloatStat(20));
                lastAttackTime = DateTime.Now;
            }
        }
    }


    
    public void OnCollision(Entity entity,Collision2D collision)
    {
        Brain brain = entity.Brain;
        if (brain.Target != null && brain.Target.gameObject == collision.gameObject) 
            isHittingTarget = true;
    }

    public void OnCollisionStopped(Entity entity,Collision2D collision)
    {
        Brain brain = entity.Brain;
        if (brain.Target != null && brain.Target.gameObject == collision.gameObject) 
            isHittingTarget = false;
    }
}
