﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(BruteLogic), menuName = "Brains/Fighting Logic/" + nameof(BruteLogic))]
public class BruteLogic : BrainLogic
{
    public float defensePeriod;
    public float attackPeriod;
    
    private GameObject shield;
    private float timer;

    public override void Think(Entity entity)
    {
        GetShieldReference(entity);
        if (shield != null)
        {
            if (timer > defensePeriod)
            {
                Attack(entity);
            }

            if (timer > defensePeriod + attackPeriod)
            {
                Shield();
                timer = 0;
            }

            timer += Time.deltaTime;
        }
    }

    private void Attack(Entity entity)
    {
        shield.SetActive(false);
        WeaponsSystem weaponsSystem = entity.WeaponsSystem;
        Brain brain = entity.Brain;
        if (!weaponsSystem.Reloaded) 
            weaponsSystem.Reload();
        weaponsSystem.Fire(brain.Target.transform.position - brain.transform.position);
    }

    private void Shield()
    {
        shield.SetActive(true);
    }
    
    private void GetShieldReference(Entity entity)
    {
        if (shield == null)
            shield = entity.gameObject.transform.Find("Shield").gameObject;
    }
}
