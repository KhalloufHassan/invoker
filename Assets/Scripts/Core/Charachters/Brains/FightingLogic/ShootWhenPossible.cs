﻿using UnityEngine;

[CreateAssetMenu(fileName =  nameof(ShootWhenPossible), menuName = "Brains/Fighting Logic/" + nameof(ShootWhenPossible))]
public class ShootWhenPossible : BrainLogic
{
    public override void Think(Entity entity)
    {
        WeaponsSystem weaponsSystem = entity.WeaponsSystem;
        Brain brain = entity.Brain;
        if(brain.Target == null)
            return;
        if (!weaponsSystem.Reloaded) 
            weaponsSystem.Reload();
        weaponsSystem.Fire(brain.Target.transform.position - brain.transform.position);
    }
}
