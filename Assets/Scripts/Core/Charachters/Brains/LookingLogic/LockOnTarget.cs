﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(LockOnTarget) , menuName = "Brains/Looking Logic/" + nameof(LockOnTarget) )]

public class LockOnTarget : BrainLogic
{
    public override void Think(Entity entity)
    {
        if(entity.Brain.Target != null)
            entity.Movement.LookAt(entity.Brain.Target.transform.position);
    }
}
