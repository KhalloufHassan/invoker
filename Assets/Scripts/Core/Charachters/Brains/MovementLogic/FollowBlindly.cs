﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(FollowBlindly) , menuName = "Brains/Movement Logic/" + nameof(FollowBlindly))]
public class FollowBlindly : BrainLogic
{
    public override void Think(Entity entity)
    {
        Brain brain = entity.Brain;
        Movement movement = entity.Movement;
        if (brain.Target != null)
        {
            var position = brain.Target.transform.position;
            movement.MoveTowards(position);
            movement.LookAt(position);
        }
    }
}
