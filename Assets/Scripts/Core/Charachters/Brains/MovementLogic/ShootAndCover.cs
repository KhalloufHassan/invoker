﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = nameof(ShootAndCover) , menuName = "Brains/Movement Logic/" + nameof(ShootAndCover))]
public class ShootAndCover : BrainLogic
{
    private Vector2 currentTargetPosition;
    private DateTime lastTargetPositionTime;

    public override void Think(Entity entity)
    {
        if (entity.Damagable.IsTakingHeavyDamage)
            Flee(entity);
        else
            ChaseTarget(entity);
    }

    private void Flee(Entity entity)
    {
        AstarAI seeker = entity.GetComponent<AstarAI>(); // TODO add to entity, too expensive
        Brain brain = entity.Brain;
        if (entity.Brain.BrainMode != BrainMode.Fleeing || lastTargetPositionTime.HasSecondsPassed(5))
        {
            seeker.ChangeTarget(brain.room.obstacles.Random().GetHidingPointFromTarget(brain.Target.transform,1));
            lastTargetPositionTime = DateTime.Now;
        }

        entity.Brain.BrainMode = BrainMode.Fleeing;
    }

    private void ChaseTarget(Entity entity)
    {
        Brain brain = entity.Brain;
        if(brain.Target == null)
            return;
        if (brain.BrainMode != BrainMode.Attacking || lastTargetPositionTime.HasSecondsPassed(2))
        {
            AstarAI seeker = entity.GetComponent<AstarAI>(); // TODO add to entity, too expensive
            var xSign = Random.value > 0.5 ? 1 : -1;
            var ySign = Random.value > 0.5 ? 1 : -1;
            var position = brain.Target.transform.position;
            currentTargetPosition = new Vector2(xSign * Random.value * 5 + position.x,ySign * Random.value * 5 + position.y);
            seeker.ChangeTarget(currentTargetPosition);
            lastTargetPositionTime = DateTime.Now;
        }

        brain.BrainMode = BrainMode.Attacking;
    }
}
