﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(FollowSmartly) , menuName = "Brains/Movement Logic/" + nameof(FollowSmartly))]
public class FollowSmartly : BrainLogic
{
    public override void Init(Entity entity)
    {
        entity.Brain.OnTargetChange += () =>
        {
            if(entity.Brain.Target != null)
                ChangeTarget(entity);
        };
    }

    public override void Think(Entity entity)
    {

    }

    private void ChangeTarget(Entity entity)
    {
        AstarAI seeker = entity.GetComponent<AstarAI>(); // TODO add to entity, too expensive
        seeker.ChangeTarget(entity.Brain.Target.transform);
    }
}
