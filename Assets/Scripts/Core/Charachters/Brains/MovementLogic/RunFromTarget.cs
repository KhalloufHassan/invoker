﻿using System;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = nameof(RunFromTarget) , menuName = "Brains/Movement Logic/" + nameof(RunFromTarget))]
public class RunFromTarget : BrainLogic
{
    public float changeTargetDuration;
    public BrainMode[] runningModes;
    private DateTime lastTargetPositionTime;

    public override void Think(Entity entity)
    {
        Brain brain = entity.Brain;
        if (runningModes.Contains(entity.Brain.BrainMode) && lastTargetPositionTime.HasSecondsPassed(changeTargetDuration))
        {
            AstarAI seeker = entity.GetComponent<AstarAI>(); // TODO add to entity, too expensive
            seeker.ChangeTarget(brain.room.obstacles.Random().GetHidingPointFromTarget(brain.Target.transform,1));
            lastTargetPositionTime = DateTime.Now;
        }
    }
}
