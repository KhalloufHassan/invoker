﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = nameof(RandomPointAroundTarget) , menuName = "Brains/Movement Logic/" + nameof(RandomPointAroundTarget))]
public class RandomPointAroundTarget : BrainLogic
{
    public float timeForChangingTargetPosition;
    private Vector2? currentTargetPosition;
    private DateTime lastTargetPositionTime;


    public override void Think(Entity entity)
    {
        Brain brain = entity.Brain;
        Movement movement = entity.Movement;
        if(lastTargetPositionTime.HasSecondsPassed(timeForChangingTargetPosition))
            FindNextPosition(brain);
        if(currentTargetPosition.HasValue)
        {
            movement.MoveTowards(currentTargetPosition.Value);
            movement.LookAt(currentTargetPosition.Value);
        }
        
    }

    private void FindNextPosition(Brain brain)
    {
        if(brain.Target == null)
            return;
        var xSign = Random.value > 0.5 ? 1 : -1;
        var ySign = Random.value > 0.5 ? 1 : -1;
        var position = brain.Target.transform.position;
        currentTargetPosition = new Vector2(xSign * Random.value * 10 + position.x,ySign * Random.value * 10 + position.y);
        lastTargetPositionTime = DateTime.Now;
    }
}
