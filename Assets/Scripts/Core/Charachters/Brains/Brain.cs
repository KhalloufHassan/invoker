﻿using System;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent,RequireComponent(typeof(Entity))]
public class Brain : Core.Systems.System
{
    [SerializeField] private Entity entity;

    [SerializeField,ReadOnlyWhenPlaying]
    private List<BrainLogic> LogicPipeLine;

    public Room room;


    private List<BrainLogic> logicPipeLine;
    public Action OnTargetChange;

    #region Brain State

    private GameObject target;
    public GameObject Target
    {
        get => target;
        set
        {
            target = value;
            OnTargetChange?.Invoke();
        }
    }
    public BrainMode BrainMode { get; set; }
    #endregion


    private void Start()
    {
        logicPipeLine = new List<BrainLogic>();
        foreach (BrainLogic logic in LogicPipeLine)
        {
            AddLogic(Instantiate(logic));
        }
    }

    private void Update()
    {
        if(IsOffline) return;
        foreach (IBrainLogic logic in logicPipeLine)
        {
            logic.Think(entity);
        }
    }

    private void AddLogic(BrainLogic logic)
    {
        if(logicPipeLine == null) logicPipeLine = new List<BrainLogic>();
        logicPipeLine.Add(logic);
        logic.Init(entity);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        foreach (BrainLogic logic in logicPipeLine)
        {
            if(logic is IBrainCollisionLogic collisionLogic)
                collisionLogic.OnCollision(entity,collision);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)   
    {
        foreach (BrainLogic logic in logicPipeLine)
        {
            if(logic is IBrainCollisionLogic collisionLogic)
                collisionLogic.OnCollisionStopped(entity,collision);//
        }
    }
}
