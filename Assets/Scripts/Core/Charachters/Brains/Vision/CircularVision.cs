﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = nameof(CircularVision) , menuName = "Brains/Vision/" + nameof(CircularVision))]
public class CircularVision : BrainLogic
{
    public float radius;

    private static readonly Collider2D[] colliders = new Collider2D[10];

    public override void Think(Entity entity)
    {
        Brain brain = entity.Brain;
        if (brain.Target != null) return;
        colliders.Clear();
        Physics2D.OverlapCircleNonAlloc(brain.transform.position, radius, colliders,LayerMask.GetMask("Friend"));
        brain.Target = colliders.FirstOrDefault(c => c != null && c.gameObject != null)?.gameObject;
    }
}
