﻿using UnityEngine;

public abstract class BrainLogic : ScriptableObject,IBrainLogic
{
    public virtual void Init(Entity entity){}
    public abstract void Think(Entity entity);
}
