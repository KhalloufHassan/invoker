﻿using UnityEngine;

public interface IBrainLogic
{
    void Think(Entity entity);
}
