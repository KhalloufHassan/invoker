﻿using UnityEngine;

public class Damagable : MonoBehaviour
{
    // the base stat of a Damagable exposed to the unity inspector
    // those should be set per prefab in the inspector
    [SerializeField] private int baseHP;
    [SerializeField] private Entity entity;
    [SerializeField] private GameEvent deathEvent;
    [SerializeField] private float currentHP;
   public bool Invulnerable { get; set; }
    
    #region Helping Stats

    private float percentageLost;
    public bool IsTakingHeavyDamage => percentageLost > 30;

    #endregion



    public float HP => currentHP;

    public float TotalHP => baseHP;

    private void Start()
    {
        currentHP = baseHP;
    }

    public void TakeDamage(FloatStat damage)
    {
        if(Invulnerable)
            return;
        currentHP -= damage.Value;
        if (HP <= 0 && entity != null)
        {
            entity.DestroyEntity();
            if(deathEvent != null)
                deathEvent.Trigger();
        }

        percentageLost += (damage.Value / TotalHP) * 100;
    }

    private void Update()
    {
        if(gameObject.name == "Invoker")
            return;
        percentageLost -= Time.deltaTime;
        percentageLost = Mathf.Max(0, percentageLost);
    }
}
