﻿using UnityEngine;

public class BasicMovement : Movement
{
    private Collider2D _collider;

    private Collider2D Collider
    {
        get
        {
            if (_collider == null)
                _collider = GetComponent<Collider2D>();
            return _collider;
        }
    }
    
    public bool isTopView; //TODO this is temp until I see what can we do with the animation and art etc
    
    public override void RotateToAngle(float angle)
    {
        if(IsOffline) return;
        transform.eulerAngles = new Vector3( 0, 0, angle );
    }

    public override void MoveTowards(Vector2 target)
    {
        if(IsOffline) return;
        // _rigidbody.velocity = ((Vector3)target - transform.position).normalized * MovementSpeed;
        _rigidbody.AddForce(((Vector3)target - transform.position).normalized * MovementSpeed);
    }
    
    public override void MoveInDirection(Vector2 direction)
    {
        if (isTakingCover) 
            MoveInCover(direction);
        else
            DoMoveInDirection(direction);

    }

    public override void LookAt(Vector2 targetPosition)
    {
        if(IsOffline) return;
        // get direction you want to point at
        var position = transform.position;
        Vector3 direction = (targetPosition - (Vector2)position).normalized;
        VisionDirection = direction;
        //direction = Vector2.MoveTowards(t.up, direction, turnRate);
        // set vector of transform directly
        //TODO uncomment this to make the sprite face the direction as well, used for top view sprites
        if(isTopView) 
            transform.up = -direction;
    }
    
    private void DoMoveInDirection(Vector2 direction)
    {
        if(direction == Vector2.zero || IsOffline)
            return;
        //TODO, testing new physics configs, create separate movement behaviour for this
        if(gameObject.GetComponent<Shot>() != null)
            _rigidbody.velocity = MovementSpeed * direction.normalized;
        else
            _rigidbody.AddForce(MovementSpeed * direction.normalized);
    }

    #region Cover Implementaion
    
    public float distanceToCheckForCover;
    // Cover Data
    private GameObject coverObject;
    private Vector2? targetCoverPoint;
    private Vector2 coverNormal;
    private bool isTouchingCover;
    private bool isTakingCover;
    
    public override void TakeCover()
    {
        isTakingCover = true;
        // if not touching a cover, look for a close cover
        if (!isTouchingCover && !targetCoverPoint.HasValue)
            FindCover();
        
        // if cover is found and we are not touching it, move to it
        if (!isTouchingCover && targetCoverPoint.HasValue)
        {
            MoveTowards(targetCoverPoint.Value);
        }
    }

    public override void LeaveCover()
    {
        isTakingCover = false;
        isTouchingCover = false;
        coverObject = null;
        targetCoverPoint = null;
    }

    private void MoveInCover(Vector2 direction)
    {
        var extent = Collider.bounds.extents.x * 0;
        var x = Physics2D.Raycast(Collider.bounds.center.AddX(extent), -coverNormal.Rotate(-45),2,LayerMask.GetMask("Walls"));
        var y = Physics2D.Raycast(Collider.bounds.center.AddX(extent), -coverNormal.Rotate(45),2,LayerMask.GetMask("Walls"));
        
        
        
        if (!x && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W)))
        {
            GetComponent<Rigidbody2D>().velocity *= 0;
            GetComponent<WeaponsSystem>().FiringPosition = Collider.bounds.center + (Vector3)coverNormal.Rotate(90);
        }
        else if (!y && (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S)))
        {
            GetComponent<Rigidbody2D>().velocity *= 0;
            GetComponent<WeaponsSystem>().FiringPosition = Collider.bounds.center + (Vector3)coverNormal.Rotate(-90);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W))
        {
            DoMoveInDirection(coverNormal.Rotate(90));
            GetComponent<WeaponsSystem>().FiringPosition = null;
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S))
        {
            DoMoveInDirection(coverNormal.Rotate(-90));
            GetComponent<WeaponsSystem>().FiringPosition = null;
        }
        else
        {
            GetComponent<WeaponsSystem>().FiringPosition = null;
        }
    }
    
    private void FindCover()
    {
        var hit = Physics2D.Raycast(gameObject.transform.position, VisionDirection,distanceToCheckForCover,LayerMask.GetMask("Walls"));
        if (hit.collider != null)
        {
            targetCoverPoint = hit.point;
            coverObject = hit.collider.gameObject;
            if (hit.collider.IsTouching(Collider))
            {
                coverNormal = hit.normal;
                isTouchingCover = true;
                targetCoverPoint = null;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject == coverObject)
        {
            coverNormal = other.contacts[0].normal;
            isTouchingCover = true;
            targetCoverPoint = null;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject == coverObject)
            isTouchingCover = false;
    }
    
    private void OnDrawGizmos()
    {
        if (isTouchingCover)
        {
            Collider2D _collider = GetComponent<Collider2D>();
            // Draws a 5 unit long red line in front of the object
            Gizmos.color = Color.red;
            Vector2 direction = -coverNormal;
            //Gizmos.DrawRay(_collider.bounds.center.AddX(_collider.bounds.extents.x), direction.Rotate(-45) * 5);
            Gizmos.DrawRay(_collider.bounds.center.AddX(_collider.bounds.extents.x * 0), direction.Rotate(-45) * 2);   
            Gizmos.DrawRay(_collider.bounds.center.AddX(_collider.bounds.extents.x * 0), direction.Rotate(45) * 2);   

        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Gizmos.color = Color.white;
            Gizmos.DrawRay(gameObject.transform.position, VisionDirection * distanceToCheckForCover);
        }
    }
    
    #endregion

}
