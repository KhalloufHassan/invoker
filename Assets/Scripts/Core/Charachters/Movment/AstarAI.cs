﻿using Pathfinding;
using UnityEngine;

public class AstarAI : Seeker
{
    /// <summary>
    /// Time window for recalculating the path to the target
    /// </summary>
    public float reCalculatePathFrequency = .5f;
    public float changeWayPointThreshold = 3;

    private Movement movement;
    private float timeToRecalculate;
    private int currentWayPoint = -1;
    private Transform currentTarget;
    
    public Vector3? CurrentWayPoint => currentWayPoint < 0 || currentWayPoint >= path.vectorPath.Count ? (Vector3?) null : path.vectorPath[currentWayPoint];
    
    public void Start ()
    {
        movement = GetComponent<Movement>();
        pathCallback += OnPathComplete;
    }

    private void OnDisable()
    {
        pathCallback -= OnPathComplete;
    }

    /// <summary>
    /// Changes the current target
    /// </summary>
    /// <param name="targetTransform">a transform for the wanted target</param>
    public void ChangeTarget(Transform targetTransform)
    {
        currentTarget = targetTransform;
        currentWayPoint = -1;
        StartPath(transform.position, targetTransform.position);
        timeToRecalculate = reCalculatePathFrequency;
    }

    public void ChangeTarget(Vector2 position)
    {
        currentTarget = null;
        currentWayPoint = -1;
        StartPath(transform.position, position);
        timeToRecalculate = reCalculatePathFrequency;
    }
    
    private void Update()
    {
        if (timeToRecalculate <= 0 && currentTarget != null)
        {
            ChangeTarget(currentTarget);
            timeToRecalculate = reCalculatePathFrequency;
        }

        if(path == null) return;
        timeToRecalculate -= Time.deltaTime;

        
        if (CurrentWayPoint.HasValue && Vector3.Distance(transform.position, CurrentWayPoint.Value) < changeWayPointThreshold)
            currentWayPoint++;
        if(CurrentWayPoint.HasValue)
            movement.MoveTowards(CurrentWayPoint.Value);
    }

    private void OnPathComplete (Path p)
    {
        currentWayPoint = 0;
    }
}
