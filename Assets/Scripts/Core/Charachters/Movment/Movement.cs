﻿using UnityEngine;

public abstract class Movement : Core.Systems.System
{
    [SerializeField] private FloatStat movementSpeed;
    [SerializeField] protected Animator _animator;
    [SerializeField] protected Rigidbody2D _rigidbody;
    
    private static readonly int X = Animator.StringToHash("x");
    private static readonly int Y = Animator.StringToHash("y");
    private static readonly int Speed = Animator.StringToHash("speed");

    private bool hasRigidbody;
    private bool hasAnimator;
    private Vector2 lastPosition;

    private void Start()
    {
        hasAnimator = _animator != null;
        hasRigidbody = _rigidbody != null;
        lastPosition = transform.position;
    }

    private void Update()
    {
        Vector2 currentPosition = transform.position;
        TotalTraveledDistance += Vector2.Distance(currentPosition, lastPosition);
        lastPosition = currentPosition;
        UpdateAnimation();
    }
    
    public Vector2 VisionDirection { get; protected set; }
    
    public FloatStat MovementSpeed
    {
        get => movementSpeed;
        set => movementSpeed = value;
    } 
    
    public float TotalTraveledDistance { get; private set; }

    /// <summary>
    /// Looks at the given point
    /// </summary>
    /// <param name="target">target point</param>
    public abstract void LookAt(Vector2 target);

    /// <summary>
    /// Moves the object towards the target point
    /// </summary>
    /// <param name="target">target point</param>
    public abstract void MoveTowards(Vector2 target);

    public abstract void RotateToAngle(float angle);

    public abstract void MoveInDirection(Vector2 direction);

    public abstract void TakeCover();

    public abstract void LeaveCover();

    private void UpdateAnimation()
    {
        if(hasAnimator)
        {
            if(hasRigidbody)
                _animator.SetFloat(Speed, _rigidbody.velocity.magnitude);
            _animator.SetFloat(X, VisionDirection.x);
            _animator.SetFloat(Y, VisionDirection.y);
        }
    }
}
