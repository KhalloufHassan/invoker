﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(GameReferences), menuName = "Game/" + nameof(GameReferences))]
public class GameReferences : ScriptableObject
{
    private GameObject player;
    private Entity _playerEntity;

    public GameObject Player
    {
        get => player;
        set
        {
            player = value;
            _playerEntity = player.GetComponent<Entity>();
        }
    }

    public Entity PlayerEntity => _playerEntity;

}
