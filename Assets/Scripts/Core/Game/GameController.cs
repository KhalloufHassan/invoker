﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameReferences GameReferences;
    public GameObject PlayerPrefab;
    public GameObject greenMonster;
    public GameObject flyingMonster;
    public GameObject bossMonster;

    private void Start()
    {
        GameReferences.Player = GameObject.FindWithTag("Player");
    }

    public void SpawnPlayer(float x, float y)
    {
        GameReferences.Player = Instantiate(PlayerPrefab, new Vector3(x + 2, y + 2, 0), Quaternion.identity);
    }

    public void SpawnGreenMonster(float x, float y)
    {
        Instantiate(greenMonster, new Vector3(x + 2, y + 2, 0), Quaternion.identity);
    }

    public void SpawnFlyingMonster(float x, float y)
    {
        Instantiate(flyingMonster, new Vector3(x + 2, y + 2, 0), Quaternion.identity);
    }

    public void SpawnBoss(float x, float y)
    {
        Instantiate(bossMonster, new Vector3(x + 2, y + 2, 0), Quaternion.identity);
    }
}
