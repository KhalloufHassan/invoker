﻿using UnityEngine;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour
{
    public GameEvent GameEvent;
    public UnityEvent Action;

    private void Awake()
    {
        GameEvent.AddListener(TriggerAction);
    }

    private void OnDestroy()
    {
        GameEvent.RemoveListener(TriggerAction);
    }

    private void TriggerAction()
    {
        Action.Invoke();
    }
}
