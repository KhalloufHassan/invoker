﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = nameof(GameEvent), menuName = "Game/" + nameof(GameEvent))]
public class GameEvent : ScriptableObject
{
    private List<Action> actions = new List<Action>();

    public void AddListener(Action listener)
    {
        actions.Add(listener);
    }

    public void RemoveListener(Action listener)
    {
        actions.Remove(listener);
    }

    [ContextMenu("Trigger")]
    public void Trigger()
    {
        actions.ForEach(a => a());
    }
}
