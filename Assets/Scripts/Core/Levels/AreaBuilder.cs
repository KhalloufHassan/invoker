﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class AreaBuilder
{
    private readonly float chanceToChangeDirection = .6f;
    private readonly int tilesToBuild;
    private readonly TileType[,] map;
    private Vector2Int position;
    private Vector2Int direction;
    private Vector2Int entranceLocation;
    private Vector2Int exitLocation;
    private List<Vector2Int> builtTiles;

    private int minX = int.MaxValue;
    private int maxX = int.MinValue;
    private int minY = int.MaxValue;
    private int maxY = int.MinValue;
    
    public AreaBuilder(Vector2Int position,int tilesToBuild,TileType[,] map)
    {
        this.tilesToBuild = tilesToBuild;
        this.position = position;
        this.map = map;
        builtTiles = new List<Vector2Int>();
    }

    public Vector2Int ExitPosition => exitLocation;

    public void BuildArea()
    {
        // build the entrance
        map[position.x, position.y] = TileType.Floor;
        builtTiles.Add(position);

        while (builtTiles.Count < tilesToBuild)
        {
            Move();
            if (map[position.x, position.y] == TileType.Empty)
            {
                map[position.x, position.y] = TileType.Floor;
                builtTiles.Add(position);
                minX = Mathf.Min(minX, position.x);
                maxX = Mathf.Max(maxX, position.x);
                minY = Mathf.Min(minY, position.y);
                maxY = Mathf.Max(maxY, position.y);
            }
        }
        
        CloseArea(true);
    }

    private void CloseArea(bool keepOneOpening)
    {
        foreach (var tileLocation in builtTiles)
        {
            if (map[tileLocation.x, tileLocation.y + 1] == TileType.Empty) map[tileLocation.x, tileLocation.y + 1] = TileType.Wall;
            if (map[tileLocation.x, tileLocation.y - 1] == TileType.Empty) map[tileLocation.x, tileLocation.y - 1] = TileType.Wall;
            if (map[tileLocation.x + 1, tileLocation.y] == TileType.Empty) map[tileLocation.x + 1, tileLocation.y] = TileType.Wall;
            if (map[tileLocation.x - 1, tileLocation.y] == TileType.Empty) map[tileLocation.x - 1, tileLocation.y] = TileType.Wall;
        }

        if (keepOneOpening) CreateExit();
    }
    
    private void CreateExit()
    {
        // pick and edge tile randomly
        //TODO OPTIMIZE, don't create new list
        int choice = Mathf.FloorToInt(Random.value * 3.99f);
        //use that int to chose a direction
        switch (choice){
            case 0:
                exitLocation = builtTiles.Skip(1).Where(t => t.y == maxY).OrderBy(t => t.x).ToList().First();
                break;
            case 1:
                exitLocation = builtTiles.Skip(1).Where(t => t.y == minY).OrderByDescending(t => t.x).ToList().First();
                break;
            case 2:
                exitLocation = builtTiles.Skip(1).Where(t => t.x == maxX).OrderBy(t => t.y).ToList().Random();
                break;
            default:
                exitLocation = builtTiles.Skip(1).Where(t => t.x == minX).OrderByDescending(t => t.y).ToList().Random();
                break;
        }

        if (map[exitLocation.x, exitLocation.y + 1] == TileType.Wall) map[exitLocation.x, exitLocation.y + 1] = TileType.Floor;
        if (map[exitLocation.x, exitLocation.y - 1] == TileType.Wall) map[exitLocation.x, exitLocation.y - 1] = TileType.Floor;
        if (map[exitLocation.x + 1, exitLocation.y] == TileType.Wall) map[exitLocation.x + 1, exitLocation.y] = TileType.Floor;
        if (map[exitLocation.x - 1, exitLocation.y] == TileType.Wall) map[exitLocation.x - 1, exitLocation.y] = TileType.Floor;
    }

    private void Move()
    {
        if (Random.value < chanceToChangeDirection) 
            direction = RandomDirection();
        position += direction;
        position.x = Mathf.Clamp(position.x, 1, map.GetLength(0)-2);
        position.y = Mathf.Clamp(position.y, 1, map.GetLength(1)-2);
    }

    private static Vector2Int RandomDirection()
    {
        int choice = Mathf.FloorToInt(Random.value * 3.99f);
        //use that int to chose a direction
        switch (choice){
            case 0:
                return Vector2Int.down;
            case 1:
                return Vector2Int.left;
            case 2:
                return Vector2Int.up;
            default:
                return Vector2Int.right;
        }
    }
}
