﻿using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class AreasMapGenerator : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Tilemap floorTileMap;
    [SerializeField] private Tilemap wallsTileMap;
    [SerializeField] private Tile floorTile;
    [SerializeField] private Tile wallTile;
    [SerializeField] private Tile specialTile;
    [SerializeField] private GameController gameController;

    [Header("Map properties")]
    [SerializeField] private int width;
    [SerializeField] private int height;

    private TileType[,] map;

    public void Start()
    {
        GenerateMap();
    }

    private void GenerateMap()
    {
        map = new TileType[width,height];
        Vector2Int pos = new Vector2Int(Random.Range(1,width-1),Random.Range(1,height-1));
        AreaBuilder startRoomBuilder = new AreaBuilder(pos,15,map);
        startRoomBuilder.BuildArea();
        AreaBuilder anotherBuilder = new AreaBuilder(startRoomBuilder.ExitPosition,40,map);
        anotherBuilder.BuildArea();
        //BuildWalls();
        FillMap(map);
        
        if(gameController.GameReferences.Player == null)
        {
            Vector3 location = floorTileMap.CellToWorld(new Vector3Int(pos.x, pos.y, 0));
            gameController.SpawnPlayer(location.x, location.y);
        }
        else
        {
            gameController.GameReferences.Player.transform.position = floorTileMap.CellToWorld(new Vector3Int(pos.x, pos.y, 0)) + new Vector3(2,2,0);
        }
        
    }
    

    private void FillMap(TileType[,] tileTypes)
    {
        for(int i = 0 ; i<width; i++)
            for (int j = 0; j < height; j++)
                FillTile(i, j, tileTypes[i, j]);
    }

    private void FillTile(int x, int y, TileType tileType)
    {
        if(tileType == TileType.Floor)
            floorTileMap.SetTile(new Vector3Int(x,y,0),floorTile );
        else if(tileType == TileType.Wall)
            wallsTileMap.SetTile(new Vector3Int(x,y,0),wallTile);
        else if (tileType == TileType.Special)
            wallsTileMap.SetTile(new Vector3Int(x,y,0),specialTile);
    }

    private void BuildWalls()
    {
        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
//                if (x == 0 || x == width-1 || y == 0 || y == height-1)
//                {
//                    map[x, y] = TileType.Wall;
//                    continue;
//                }
                if (map[x, y] != TileType.Floor) continue;
                if (map[x, y + 1] == TileType.Empty) map[x, y + 1] = TileType.Wall;
                if (map[x, y - 1] == TileType.Empty)  map[x, y - 1] = TileType.Wall;
                if (map[x + 1, y] == TileType.Empty) map[x + 1, y] = TileType.Wall;
                if (map[x - 1, y] == TileType.Empty) map[x - 1, y] = TileType.Wall;
            }
        }
    }

    public void Regenerate()
    {
        floorTileMap.ClearAllTiles();
        wallsTileMap.ClearAllTiles();
        GenerateMap();
    }
}
