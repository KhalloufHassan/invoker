﻿using UnityEngine;

public class RoomsLevelGenerator : MonoBehaviour
{
    public GameObject FloorPrefab;
    public GameObject WallPrefab;

    private void Start()
    {
        CreateRoom(1,1);
    }

    private void CreateRoom(int width,int height)
    {
        SpriteRenderer floor = Instantiate(FloorPrefab).GetComponent<SpriteRenderer>();
        var size = floor.size;
        size = new Vector2(width * size.x,height * size.y);
        floor.size = size;
        
        // Generate walls
        SpriteRenderer wall = Instantiate(WallPrefab).GetComponent<SpriteRenderer>();
        size = wall.size;
        size = new Vector2(width * size.x,size.y);
        wall.size = size;
    }
}
