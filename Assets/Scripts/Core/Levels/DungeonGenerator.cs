﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class DungeonGenerator : MonoBehaviour
{

    public int roomAreaWidth = 10;
    public int roomAreaHeight = 10;

    public Tilemap floorTileMap;
    public Tile floorTile;
    public Tile wallTile;
    
    public void Start()
    {
        Generate();
    }

    [ContextMenu("Generate")]
    public void Generate()
    {
        floorTileMap.ClearAllTiles();
        RoomInfo firstRoom = new RoomInfo { Width = 8,Height = 7,OtherRooms = new Dictionary<Direction, RoomInfo>()
        {
            [Direction.Right] = new RoomInfo { Width = 4,Height = 6},
            [Direction.Top] = new RoomInfo { Width = 7,Height = 8}
        }};
        GenerateRoom(0, 0,firstRoom);
    }

    private void GenerateRoom(int areaX,int areaY,RoomInfo roomInfo)
    {
        int areaStartX = areaX * roomAreaWidth;
        int areaStartY = areaY * roomAreaHeight;
        int startHor;
        int startVer;
        if (roomInfo.Height < roomAreaHeight / 2f)
            startVer = Random.Range(roomAreaHeight / 2 - roomInfo.Height + 1, roomAreaHeight / 2 + 1) + areaStartY;
        else
            startVer = Random.Range(0, Math.Min(roomAreaHeight / 2, roomAreaHeight - roomInfo.Height + 1)) + areaStartY;

        if (roomInfo.Width < roomAreaWidth / 2f)
            startHor = Random.Range(roomAreaWidth / 2 - roomInfo.Width + 1, roomAreaWidth / 2 + 1) + areaStartX;
        else
            startHor = Random.Range(0, Math.Min(roomAreaWidth / 2, roomAreaWidth - roomInfo.Width + 1)) + areaStartX;
        for (int i = startHor; i < roomInfo.Width + startHor; i++)
        {
            for (int j = startVer; j < roomInfo.Height + startVer; j++)
            {
                floorTileMap.SetTile(new Vector3Int(i,j,0), floorTile);
            }
        }
        
        foreach (KeyValuePair<Direction,RoomInfo> adjRoom in roomInfo.OtherRooms)
        {
            // build a bridge from this rooms center to the next room center
            GenerateBridge(areaX, areaY, adjRoom.Key);

            // build the other room
            int newAreaX = areaX;
            int newAreaY = areaY;
            if (adjRoom.Key == Direction.Right) newAreaX++;
            if (adjRoom.Key == Direction.Left) newAreaX--;
            if (adjRoom.Key == Direction.Top) newAreaY++;
            if (adjRoom.Key == Direction.Bottom) newAreaY--;
            GenerateRoom(newAreaX,newAreaY,adjRoom.Value);
        }

        // Testing
        //floorTileMap.SetTile(new Vector3Int(roomAreaWidth/2 + areaStartX,roomAreaHeight/2 + areaStartY,0), wallTile);
        
        // for (int i = areaStartX; i < roomAreaWidth + areaStartX; i++)
        // {
        //     floorTileMap.SetTile(new Vector3Int(i,roomAreaHeight/2 + areaStartY,0), wallTile);
        // }
        //
        // for (int i = areaStartY; i < roomAreaHeight + areaStartY; i++)
        // {
        //     floorTileMap.SetTile(new Vector3Int(roomAreaWidth/2 + areaStartX,i,0), wallTile);
        // }
    }

    private void GenerateBridge(int areaX, int areaY, Direction direction)
    {
        int areaStartX = areaX * roomAreaWidth;
        int areaStartY = areaY * roomAreaHeight;
        int bridgeLength = direction == Direction.Right || direction == Direction.Left ? roomAreaWidth : roomAreaHeight;
        Vector3Int center = new Vector3Int(roomAreaWidth / 2 + areaStartX, roomAreaHeight / 2 + areaStartY, 0);
        Vector3Int increment = direction == Direction.Right ? Vector3Int.right : direction == Direction.Left ? Vector3Int.left : direction == Direction.Top ? Vector3Int.up : Vector3Int.down;

        for (int i = 0; i < bridgeLength; i++)
        {
            floorTileMap.SetTile(center, floorTile);
            center += increment;
        }
    }

    private void OnDrawGizmos()
    {
        int roomAreas = 10;
        for (int i = -roomAreas + 1; i < roomAreas; i++)
        {
            Gizmos.color = i == 0 ? Color.white : Color.red;
            Gizmos.DrawLine(new Vector3(i * 4 * roomAreaWidth,-roomAreas * 4 * (roomAreaHeight - 1)), new Vector3(i * 4 * roomAreaWidth,roomAreas * 4 * (roomAreaHeight - 1)));
        }
        
        for (int i = -roomAreas + 1; i < roomAreas; i++)
        {
            Gizmos.color = i == 0 ? Color.white : Color.red;
            Gizmos.DrawLine(new Vector3(-roomAreas * 4 * (roomAreaWidth - 1),i * 4 * roomAreaHeight), new Vector3(roomAreas * 4 * (roomAreaWidth - 1),i * 4 * roomAreaHeight));
        }
    }
}

public enum Direction
{
    Top,
    Right,
    Left,
    Bottom
}

public class RoomInfo
{
    public int Width { get; set; }
    public int Height { get; set; }

    public Dictionary<Direction, RoomInfo> OtherRooms { get; set; } = new Dictionary<Direction, RoomInfo>();

}
