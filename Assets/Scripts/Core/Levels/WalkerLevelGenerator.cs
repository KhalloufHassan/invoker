﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class WalkerLevelGenerator : MonoBehaviour
{
    public GameController gameController;
    public Tilemap floorTileMap;
    public Tilemap wallsTileMap;
    public Tile floorTile;
    public Tile wallTile;
    public int mapWidth;
    public int mapHeight;
    
    // Tweaking values
    public int maxIterations;
    public float percentageToFill;
    public float chanceToChangeWalkerDirection;
    public float chanceToSpawnWalker;
    public float chanceToKillWalker;
    public float generationStepDuration;
    public bool slowGeneration;

    // Helpers and shortcuts
    private static Vector3Int RandomDirection()
    {
        int choice = Mathf.FloorToInt(Random.value * 3.99f);
        //use that int to chose a direction
        switch (choice){
            case 0:
                return Vector3Int.down;
            case 1:
                return Vector3Int.left;
            case 2:
                return Vector3Int.up;
            default:
                return Vector3Int.right;
        }
    }
    private Walker NewWalkerInstance => _walkers.Count == 0 ?  new Walker { position = new Vector3Int(mapWidth/2,mapHeight/2,0) , direction = RandomDirection()} : new Walker{position = _walkers[0].position, direction = RandomDirection()};
    private bool IsPercentageFulfilled => (float) floorTilesCreated / (mapHeight * mapWidth) > percentageToFill;


    private readonly List<Walker> _walkers = new List<Walker>();
    private int floorTilesCreated;
    private Vector2Int bottomLeftCorner = new Vector2Int(int.MaxValue,int.MaxValue);
    private Vector2Int bottomRightCorner = new Vector2Int(int.MinValue,int.MaxValue);
    private Vector2Int topLeftCorner = new Vector2Int(int.MaxValue,int.MinValue);
    private Vector2Int topRightCorner = new Vector2Int(int.MinValue,int.MinValue);


    private void Start()
    {
        StartCoroutine(GenerateMap());
    }

    private IEnumerator GenerateMap()
    {
        // loop until we reach max iterations
        for (int i = 0; i < maxIterations; i++)
        {
            // try spawn walker
            if (Random.value < chanceToSpawnWalker || _walkers.Count == 0) 
                _walkers.Add(NewWalkerInstance);

            foreach (Walker walker in _walkers)
            {
                if (Random.value < chanceToChangeWalkerDirection)
                    walker.direction = RandomDirection();
                walker.Move(mapWidth - 1,mapHeight - 1);
                
                // Spawn Floor in position of walkers
                if (!floorTileMap.HasTile(walker.position))
                {
                    floorTileMap.SetTile(walker.position,floorTile);
                    floorTilesCreated++;
                    UpdateCorners(walker.position.x,walker.position.y);
                    if(slowGeneration) yield return new WaitForSeconds(generationStepDuration);
                }
            }
            
            // chance to kill one walker
            if(Random.value < chanceToKillWalker && _walkers.Count > 1) 
                _walkers.RemoveAt(Random.Range(0, _walkers.Count - 1));

            // if percentage to fill is fulfilled, stop looping
            if(IsPercentageFulfilled)
            {
                print($"finished in {i} iterations and {_walkers.Count} walkers and {floorTilesCreated} tiles created");
                break;
            }
        }
        _walkers.Clear();
        StartCoroutine(CreateWalls());
    }
    
    private IEnumerator CreateWalls(){
        for (int x = 0; x < mapWidth; x++){
            for (int y = 0; y < mapHeight; y++){
                if (!floorTileMap.HasTile(new Vector3Int(x, y, 0))) continue;
                bool placed = false;
                //if any surrounding spaces are empty, place a wall
                if (!floorTileMap.HasTile(new Vector3Int(x,y+1,0))){
                    wallsTileMap.SetTile(new Vector3Int(x,y+1,0),wallTile);
                    placed = true;
                }
                if (!floorTileMap.HasTile(new Vector3Int(x,y-1,0))){
                    wallsTileMap.SetTile(new Vector3Int(x,y-1,0),wallTile);
                    placed = true;
                }
                if (!floorTileMap.HasTile(new Vector3Int(x+1,y,0))){
                    wallsTileMap.SetTile(new Vector3Int(x+1,y,0),wallTile);
                    placed = true;
                }
                if (!floorTileMap.HasTile(new Vector3Int(x-1,y,0))){
                    wallsTileMap.SetTile(new Vector3Int(x-1,y,0),wallTile);
                    placed = true;
                }
                if (placed && slowGeneration){
                    yield return new WaitForSeconds(generationStepDuration);
                }
            }
        }
        GenerateMonsters();
    }

    private void GenerateMonsters()
    {
        Vector3 location = floorTileMap.CellToWorld(new Vector3Int(bottomRightCorner.x,bottomRightCorner.y, 0));
        gameController.SpawnPlayer(location.x,location.y);
        
        location = floorTileMap.CellToWorld(new Vector3Int(bottomLeftCorner.x,bottomLeftCorner.y, 0));
        gameController.SpawnGreenMonster(location.x,location.y);
        gameController.SpawnGreenMonster(location.x,location.y);
        gameController.SpawnGreenMonster(location.x,location.y);
        
        location = floorTileMap.CellToWorld(new Vector3Int(topLeftCorner.x,topLeftCorner.y, 0));
        gameController.SpawnFlyingMonster(location.x,location.y);
        gameController.SpawnGreenMonster(location.x,location.y);
        
        location = floorTileMap.CellToWorld(new Vector3Int(topRightCorner.x,topRightCorner.y, 0));
        gameController.SpawnGreenMonster(location.x,location.y);
        gameController.SpawnGreenMonster(location.x,location.y);
        gameController.SpawnGreenMonster(location.x,location.y);
    }

    private void UpdateCorners(int x, int y)
    {
        if (x < bottomLeftCorner.x || x < topLeftCorner.x)
        {
            bottomLeftCorner = topLeftCorner = new Vector2Int(x, y);
        }
        else if (x == bottomLeftCorner.x || x == topLeftCorner.x)
        {
            bottomLeftCorner.y = Mathf.Min(bottomLeftCorner.y, y);
            topLeftCorner.y = Mathf.Max(topLeftCorner.y, y);
        }

        if (x > bottomRightCorner.x || x > topRightCorner.x)
        {
            bottomRightCorner = topRightCorner = new Vector2Int(x, y);
        }
        else if (x == bottomRightCorner.x || x == topRightCorner.x)
        {
            bottomRightCorner.y = Mathf.Min(bottomRightCorner.y);
            topRightCorner.y = Mathf.Max(topRightCorner.y, y);
        }
    }

    private class Walker
    {
        public Vector3Int position;
        public Vector3Int direction;

        public void Move(int maxX,int maxY)
        {
            position += direction;
            position.x = Mathf.Clamp(position.x, 1, maxX);
            position.y = Mathf.Clamp(position.y, 1, maxY);
        }
    }
}
