﻿using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private Tilemap _tileMap;
    [SerializeField] private Tile _tile;

    [SerializeField] private int height;
    [SerializeField] private int width;
    [SerializeField] private int roomsWidth;
    [SerializeField] private int roomsHeight;
    
    private void Start()
    {
        StartCoroutine(SeedMap());
    }

    private IEnumerator SeedMap()
    {
        for (int rw = 0; rw < roomsWidth; rw++)
        {
            for (int rh = 0; rh < roomsHeight; rh++)
            {
                int hShift = height * rh;
                int vShift = width * rw;
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        bool tiled;
                        tiled = j == 0 || j == height-1 || i == 0 || i == width-1 || Random.value > .90f;
                        if(tiled)
                        {
                            _tileMap.SetTile(new Vector3Int(j + vShift, i + hShift, 0), _tile);
                            yield return new WaitForSeconds(.01f);
                        }
                    }
                }
            }
        }
    }
}
