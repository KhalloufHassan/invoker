﻿using System;
using JetBrains.Annotations;
using UnityEngine;

[Serializable]
public struct IntStat
{

    [SerializeField]
    private int baseValue;

    [SerializeField]
    private float multiplier;

    public int MinValue;
    public int Value
    {
        get => (int) Math.Max(MinValue,baseValue * (1 + multiplier));
        set => baseValue = value;
    }

    public IntStat(int value,float multiplier = 0,int minValue = 0)
    {
        baseValue = value;
        MinValue = minValue;
        this.multiplier = multiplier;
    }

    [Pure]
    public IntStat AddMultiplier(float mult)
    {
        return new IntStat(baseValue,multiplier + mult,MinValue);
    }

    public override string ToString()
    {
        return $"Value: {Value} | Base Value: {baseValue} | Multiplier: {multiplier} | MinValue: {MinValue}";
    }

    #region float Operators Override

    public static implicit operator int(IntStat stat)
    {
        return stat.Value;
    }

    public static implicit operator IntStat(int val)
    {
        return new IntStat(val);
    }
    
    public static IntStat operator +(IntStat s1, int s2)
    {
        return new IntStat(s1.baseValue + s2,s1.multiplier,s1.MinValue);
    }

    public static IntStat operator -(IntStat s1, int s2)
    {
        return new IntStat(s1.baseValue - s2,s1.multiplier,s1.MinValue);
    }
    

    #endregion

    #region Stat Operators Override
    
    public static IntStat operator +(IntStat s1, IntStat s2)
    {
        return new IntStat(s1.baseValue + s2.baseValue,s1.multiplier + s2.multiplier,Math.Max(s1.MinValue,s2.MinValue));
    }
    
    public static IntStat operator -(IntStat s1, IntStat s2)
    {
        return new IntStat(s1.baseValue - s2.baseValue,s1.multiplier - s2.multiplier,Math.Max(s1.MinValue,s2.MinValue));
    }
    
    #endregion
}