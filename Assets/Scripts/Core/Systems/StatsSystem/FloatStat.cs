﻿using System;
using JetBrains.Annotations;
using UnityEngine;

[Serializable]
public struct FloatStat
{

    [SerializeField]
    private float baseValue;

    [SerializeField]
    private float multiplier;

    public float MinValue;
    public float Value
    {
        get => Math.Max(MinValue,baseValue * (1 + multiplier));
        set => baseValue = value;
    }

    public FloatStat(float value,float multiplier = 0,float minValue = 0)
    {
        baseValue = value;
        MinValue = minValue;
        this.multiplier = multiplier;
    }

    [Pure]
    public FloatStat AddMultiplier(float mult)
    {
        return new FloatStat(baseValue,multiplier + mult,MinValue);
    }

    public override string ToString()
    {
        return $"Value: {Value} | Base Value: {baseValue} | Multiplier: {multiplier} | MinValue: {MinValue}";
    }

    #region float Operators Override

    public static implicit operator float(FloatStat stat)
    {
        return stat.Value;
    }

    public static implicit operator FloatStat(float val)
    {
        return new FloatStat(val);
    }
    
    public static FloatStat operator +(FloatStat s1, float s2)
    {
        return new FloatStat(s1.baseValue + s2,s1.multiplier,s1.MinValue);
    }

    public static FloatStat operator -(FloatStat s1, float s2)
    {
        return new FloatStat(s1.baseValue - s2,s1.multiplier,s1.MinValue);
    }
    

    #endregion

    #region Stat Operators Override
    
    public static FloatStat operator +(FloatStat s1, FloatStat s2)
    {
        return new FloatStat(s1.baseValue + s2.baseValue,s1.multiplier + s2.multiplier,Math.Max(s1.MinValue,s2.MinValue));
    }
    
    public static FloatStat operator -(FloatStat s1, FloatStat s2)
    {
        return new FloatStat(s1.baseValue - s2.baseValue,s1.multiplier - s2.multiplier,Math.Max(s1.MinValue,s2.MinValue));
    }
    
    #endregion
}