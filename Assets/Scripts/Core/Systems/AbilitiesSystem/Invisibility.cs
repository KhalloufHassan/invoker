﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class Invisibility : MonoBehaviour
{
    public Entity entity;
    public float duration;
    public BreakInvisibilityMod mod;

    private float timer;
    private bool isActive;
    private static readonly Collider2D[] colliders = new Collider2D[50]; // to avoid garbage collection

    // Update is called once per frame
    private void Update()
    {
        if (!isActive)
        {
            if(Input.GetKeyDown(KeyCode.LeftControl))
                Activate();
            return;
        }
        else
        {
            timer += Time.deltaTime;
            if (timer >= duration)
                DeActivate();
        }
    }

    public void DeActivate()
    {
        isActive = false;
        gameObject.layer = LayerMask.NameToLayer("Friend");
        var sr = gameObject.GetComponent<SpriteRenderer>();
        var color = sr.color;
        color = new Color(color.r,color.g,color.b,1f);
        sr.color = color;
        StartCoroutine(DeAttach());
    }

    private IEnumerator DeAttach()
    {
        yield return new WaitForEndOfFrame();
        entity.WeaponsSystem.DetachMod(mod);
    }

    private void Activate()
    {
        isActive = true;
        timer = 0;
        colliders.Clear();
        var sr = gameObject.GetComponent<SpriteRenderer>();
        var color = sr.color;
        color = new Color(color.r,color.g,color.b,.5f);
        sr.color = color;
        Physics2D.OverlapCircleNonAlloc(gameObject.transform.position, 10000, colliders,LayerMask.GetMask("Enemy"));
        foreach (var go in colliders.Select(c => c == null? null : c.gameObject))
        {
            if(go == null) continue;
            if (go.GetComponent<Brain>() != null)
                go.GetComponent<Brain>().Target = null;
            if(go.GetComponent<AstarAI>() != null)
                go.GetComponent<AstarAI>().ChangeTarget(go.transform.position);
        }
        gameObject.layer = LayerMask.NameToLayer("Invis");
        entity.WeaponsSystem.AttachMod(mod);
    }
}
