﻿using System.Linq;
using UnityEngine;

public class BioticCharge : MonoBehaviour
{
    public Entity entity;
    public Effect stunEffect;
    public ParticleSystem ps;
    public float aoeRadius = 5;
    public float pushForce = 1000;
    public float speed = 40;

    private readonly Collider2D[] colliders = new Collider2D[10]; // to avoid garbage collection
    private float time;
    private bool isActive;
    private Rigidbody2D rigidBody;

    private void Start()
    {
        rigidBody = entity.GetComponent<Rigidbody2D>();
    }

    private void Activate()
    {
        if(isActive) return;
        entity.Movement.IsOffline = true;
        entity.Damagable.Invulnerable = true;
        time = 0;
        isActive = true;
    }

    private void Disable()
    {
        if(!isActive) return;
        entity.Movement.IsOffline = false;
        entity.Damagable.Invulnerable = false;
        isActive = false;
        
        colliders.Clear();
        Physics2D.OverlapCircleNonAlloc(entity.gameObject.transform.position, aoeRadius, colliders);
        foreach (var go in colliders.Select(c => c == null? null : c.gameObject))
        {
            if (go != null && go != gameObject)
            {
                if(go.GetComponent<Shot>() != null)
                    Destroy(go);
                else
                {
                    Entity e = go.GetComponent<Entity>();
                    if (e != null)
                    {
                        e.AddEffect(stunEffect);
                        Rigidbody2D rb = e.GetComponent<Rigidbody2D>();
                        if(rb != null)
                            rb.AddForce((go.transform.position - gameObject.transform.position).normalized * pushForce);
                    }
                }
            }
        }

        ps.transform.position = transform.position;
        var mainModule = ps.main;
        mainModule.startSize = aoeRadius * 2.5f;
        ps.Play();
    }

    private void Update()
    {
        if (!isActive)
        {
            if(Input.GetKeyDown(KeyCode.LeftControl))
                Activate();
            return;
        }
        
        if (time > .35f) 
            rigidBody.velocity = speed * entity.Movement.VisionDirection.normalized;
        time += Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.GetComponent<Shot>() == null)
            Disable();
    }
    
    private void OnDrawGizmos()
    {
        var transform1 = transform;
        var localScale = transform1.localScale;
        GizmosExtensions.DrawEllipse(transform1.position, transform1.forward, transform1.up, aoeRadius * localScale.x, aoeRadius * localScale.y, 32, Color.red);
    }
}
