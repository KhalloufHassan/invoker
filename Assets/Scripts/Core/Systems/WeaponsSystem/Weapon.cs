﻿using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : ScriptableObject
{
    public List<Effect> effects;
    public FloatStat damage;
    public FloatStat range;
    public FloatStat shotSpeed;
    public FloatStat fireRate = new FloatStat(0,0,0.01f);
    public IntStat shotCount = 1;
    public int spreadAngle = 15;
    public bool automatic;

    public abstract void Fire(WeaponsSystem weaponsSystem, Vector2? direction,Vector2? position);
}
