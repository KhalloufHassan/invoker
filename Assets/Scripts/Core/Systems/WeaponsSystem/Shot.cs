﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : Entity
{
    [SerializeField] private FloatStat m_damage;


    public FloatStat Damage
    {
        get => m_damage;
        set => m_damage = value;
    }
    
    public Vector2 Direction { get; set; } = Vector2.up;
    public FloatStat Range;
    private HashSet<int> preventDestructionIds;
    

    protected override void Update()
    {
        base.Update();
        Movement.MoveInDirection(Direction);
        
        if (Movement != null && Movement.TotalTraveledDistance > Range)
        {
            DestroyEntity();
        }
    }

    protected override void OnHit(Collision2D collision)
    {
        base.OnHit(collision);
        if(collision.collider.gameObject.name == collision.gameObject.name)
            collision.gameObject.GetComponent<Damagable>()?.TakeDamage(Damage);
        if (preventDestructionIds == null || preventDestructionIds.Count == 0)
        {
            StartCoroutine(Destroy());
        }
    }

    public void PreventDestruction(Effect effect)
    {
        if(preventDestructionIds == null) preventDestructionIds = new HashSet<int>();
        preventDestructionIds.Add(effect.GetInstanceID());
    }

    public void AllowDestruction(Effect effect)
    {
        if(preventDestructionIds == null) preventDestructionIds = new HashSet<int>();
        preventDestructionIds.Remove(effect.GetInstanceID());
    }

    private IEnumerator Destroy()
    {
        // destroy in the next frame to be sure all scripts interested in the shot at the collision frame are done
        yield return new WaitForEndOfFrame();
        DestroyEntity();
    }
}
