﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = nameof(PlasmaCaster), menuName = "Weapons/" + nameof(PlasmaCaster))]
public class PlasmaCaster : Weapon
{
    public Shot shotPrefab;

    public override void Fire(WeaponsSystem weaponsSystem,Vector2? direction,Vector2? position)
    {
        int finalShotCount = shotCount + weaponsSystem.shotCount;
        float intensity = 1f/finalShotCount;
        float totalSpreadAngle = spreadAngle * (finalShotCount - 1);
        float stepAngle = totalSpreadAngle / (finalShotCount - 1);
        float shootingAngle = -totalSpreadAngle/2;
        if (!position.HasValue)
            position = weaponsSystem.transform.position;
        for (int i = 0; i < finalShotCount; i++)
        {
            if (i != 0)
                shootingAngle += stepAngle;
            Vector3 currentDirection = direction ?? weaponsSystem.transform.up;
            Vector3 rotatedVector = direction.HasValue ? Quaternion.Euler(0, 0, shootingAngle) * direction.Value : Quaternion.Euler(0, 0, shootingAngle) * currentDirection;
            MakePlasmaShot(weaponsSystem, rotatedVector,intensity,position.Value);
        }
    }

    private void MakePlasmaShot(WeaponsSystem weaponsSystem,Vector2 direction,float intensity,Vector2 position)
    {
        // calculate stats for the shot
        float finalShotSpeed = weaponsSystem.ShotSpeed + shotSpeed;
        float finalDamage = weaponsSystem.Damage + damage;
        
        Shot shot = Instantiate(shotPrefab);
        var owner = weaponsSystem.gameObject;
        shot.Range = weaponsSystem.Range + range;
        shot.Movement.MovementSpeed = finalShotSpeed;
        shot.Damage = finalDamage;
        shot.Direction = direction;
        shot.transform.position = position;
        shot.gameObject.layer = LayerMask.LayerToName(owner.layer) == "Friend" ? LayerMask.NameToLayer("FriendShots") : LayerMask.NameToLayer("EnemyShots");
        var allEffects = weaponsSystem.effectsCaster != null ? effects.Concat(weaponsSystem.effectsCaster.CurrentEffects) : effects;
        foreach (Effect effect in allEffects.Distinct())
        {
            shot.AddEffect(effect,effect.IsNegativeEffect,allEffects.Count(e => e.Equals(effect)),intensity);
        }
    }
    
}
