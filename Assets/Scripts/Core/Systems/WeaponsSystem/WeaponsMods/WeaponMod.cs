﻿using UnityEngine;

public abstract class WeaponMod : ScriptableObject
{
    public string modName;
    public Sprite icon;

    public abstract void OnAttached(WeaponsSystem weaponsSystem);
    public abstract void OnDetached(WeaponsSystem weaponsSystem);
    public abstract void OnFire(WeaponsSystem weaponsSystem);
}
