using UnityEngine;

[CreateAssetMenu(fileName = nameof(BreakInvisibilityMod),menuName = "Weapons/Mods/" + nameof(BreakInvisibilityMod))]

public class BreakInvisibilityMod : WeaponMod
{
    public override void OnAttached(WeaponsSystem weaponsSystem)
    {
        weaponsSystem.Damage = weaponsSystem.Damage.AddMultiplier(2);
    }

    public override void OnDetached(WeaponsSystem weaponsSystem)
    {
        weaponsSystem.Damage = weaponsSystem.Damage.AddMultiplier(-2);
    }

    public override void OnFire(WeaponsSystem weaponsSystem)
    {
        weaponsSystem.GetComponent<Invisibility>().DeActivate();
    }
}