﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(WeaponStatMod),menuName = "Weapons/Mods/" + nameof(WeaponStatMod))]
public class WeaponStatMod : WeaponMod
{
    public FloatStat Damage = 0;
    public FloatStat Range = 0;
    public FloatStat ShotSpeed = 0;
    public FloatStat FireRate = new FloatStat(0,0.01f);
    public IntStat shotCount = 0;
    public bool automatic;
    
    public override void OnAttached(WeaponsSystem weaponsSystem)
    {
        weaponsSystem.Damage += Damage;
        weaponsSystem.Range += Range;
        weaponsSystem.ShotSpeed += ShotSpeed;
        weaponsSystem.FireRate += FireRate;
        weaponsSystem.shotCount += shotCount;
        if(automatic)
            weaponsSystem.automatic = automatic;
    }

    public override void OnDetached(WeaponsSystem weaponsSystem)
    {
        weaponsSystem.Damage -= Damage;
        weaponsSystem.Range -= Range;
        weaponsSystem.ShotSpeed -= ShotSpeed;
        weaponsSystem.FireRate -= FireRate;
        weaponsSystem.shotCount -= shotCount;
        weaponsSystem.automatic = weaponsSystem.currentWeapon.automatic;
    }

    public override void OnFire(WeaponsSystem weaponsSystem)
    {
        
    }
}
