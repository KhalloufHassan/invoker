﻿using System.Collections.Generic;
using UnityEngine;

public class WeaponsSystem : Core.Systems.System
{

    #region System Stats

    public int level = 1;
    public FloatStat Damage = 0;
    public FloatStat Range = 0;
    public FloatStat ShotSpeed = 0;
    public FloatStat FireRate = new FloatStat(0,0.01f);
    public IntStat shotCount = 0;
    public bool automatic;//TODO not a weapon stat! make the mod reloads on fire

    #endregion

    #region Weapons

    public EffectsCaster effectsCaster;
    public List<Weapon> weapons;
    public Weapon currentWeapon;
    
    #endregion

    #region System State

    private bool _reloaded = true;
    
    public List<WeaponMod> AttachedMods = new List<WeaponMod>();
    public float FireCoolDown { get; set; }
    public bool OnCoolDown => FireCoolDown > 0;
    public Vector3? FiringPosition { get; set; }

    public bool Reloaded
    {
        get => _reloaded || automatic || currentWeapon.automatic;
        private set => _reloaded = value;
    }
    public bool CanFire => !OnCoolDown && Reloaded && !IsOffline;

    #endregion

    private void Start()
    {
        foreach (WeaponMod mod in AttachedMods)
        {
            mod.OnAttached(this);
        }
    }

    private void Update()
    {
        FireCoolDown -= Time.deltaTime;
    }

    public void AddWeapon(Weapon weapon)
    {
        if(weapons.Count >= level)
            return;
        weapons.Add(weapon);
        if (currentWeapon == null)
            currentWeapon = weapon;
    }

    public void EquipWeapon(int slot)
    {
        if(slot > weapons.Count - 1)
            return;
        currentWeapon = weapons[slot];
    }
    
    public void Fire(Vector3? direction = null)
    {
        if (!CanFire) return;
        foreach(WeaponMod mod in AttachedMods)
            mod.OnFire(this);
        currentWeapon.Fire(this,direction,FiringPosition);
        FireCoolDown = 1 / (FireRate + currentWeapon.fireRate);
        Reloaded = false;
    }

    public void Reload() => Reloaded = true;

    public void AttachMod(WeaponMod mod)
    {
        //TODO make a copy of the ScriptableObject
        AttachedMods.Add(mod);
        mod.OnAttached(this);
    }

    public void DetachMod(WeaponMod mod)
    {
        if(AttachedMods.Remove(mod))
            mod.OnDetached(this);
    }
}
