﻿using UnityEngine;

[CreateAssetMenu( fileName = nameof(BuffItem), menuName = "Items/" + nameof(BuffItem))]
public class BuffItem : Item
{
    public Effect ReverseSingularity;
    public override void Use(ItemsSystem user,ItemState state)
    {
        base.Use(user, state);
        user.GetComponent<Entity>().AddEffect(ReverseSingularity);
    }
}
