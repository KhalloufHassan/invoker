﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(Shield), menuName = "Items/" + nameof(Shield))]
public class Shield : Item
{
    public GameObject ShieldPrefab;
    public override void Use(ItemsSystem user,ItemState state)
    {
        base.Use(user,state);
        //state.EntityStore.Add(Instantiate(ShieldPrefab,user.transform,false),user);
    }

    public override void OnDurationEnded(ItemsSystem user, ItemState state)
    {
        base.OnDurationEnded(user, state);
        //Destroy(state.EntityStore.Remove<GameObject>(user));
    }
}
