﻿using System;

[Serializable]
public class ItemState
{
    public Item Item;
    public float CoolDown;
    public float TimeActive;
    public bool IsActive;
}
