﻿using System.Collections.Generic;
using UnityEngine;

public class ItemsSystem : MonoBehaviour
{
    public List<ItemState> Items;

    public void UseItem(int index)
    {
        if(index > Items.Count - 1)
            return;
        ItemState state = Items[index];
        if(state.CoolDown > 0)
            return;
        state.Item.Use(this,state);
        state.CoolDown = state.Item.CoolDownDuration;
    }

    private void Update()
    {
        foreach (ItemState state in Items)
        {
            if (state.IsActive)
            {
                state.TimeActive += Time.deltaTime;
                if (state.TimeActive > state.Item.Duration)
                {
                    state.Item.OnDurationEnded(this,state);
                    state.TimeActive = 0;
                }
            }
            if (state.CoolDown > 0)
            {
                state.CoolDown -= Time.deltaTime;
                if (state.CoolDown <= 0)
                {
                    state.CoolDown = 0;
                }
            }

        }
    }
}
