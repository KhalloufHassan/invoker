﻿using UnityEngine;

public abstract class Item : ScriptableObject
{
    public float CoolDownDuration;
    public float Duration;

    public virtual void Use(ItemsSystem user, ItemState state)
    {
        state.IsActive = true;
    }

    public virtual void OnDurationEnded(ItemsSystem user,ItemState state)
    {
        state.IsActive = false;
    }
}
