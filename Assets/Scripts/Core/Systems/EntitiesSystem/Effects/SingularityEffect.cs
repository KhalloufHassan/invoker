﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu( fileName = nameof(SingularityEffect) , menuName = "Effects/" +  nameof(SingularityEffect))]
//TODO not so efficient dude
public class SingularityEffect : Effect,IOnUpdateListener
{
    public float power = 0.02f;
    public float aoeRadius = 5;
    private static readonly Collider2D[] colliders = new Collider2D[10]; // to avoid garbage collection

    private float TotalPower => power * stack * intensity;
    
    private static bool CanPull(GameObject go,Entity entity)
    {
        if (go == null || go == entity.gameObject)
            return false;
        return go.GetComponent<Damagable>() != null;
    }

    public void OnUpdate(Entity entity)
    {
        colliders.Clear();
        var gameObject = entity.gameObject;
        Physics2D.OverlapCircleNonAlloc(gameObject.transform.position, aoeRadius, colliders,LayerMask.GetMask("Enemy"));
        foreach (var go in colliders.Select(c => c == null? null : c.gameObject))
        {
            if (CanPull(go, entity))
            {
                go.GetComponent<Rigidbody2D>().AddForce((entity.transform.position - go.transform.position) * (TotalPower));
                Debug.Log($"pulling {go.name} with {TotalPower} force");
            }
        }
    }
}
