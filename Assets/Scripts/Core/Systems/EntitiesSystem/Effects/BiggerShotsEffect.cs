﻿using UnityEngine;

[CreateAssetMenu( fileName = nameof(BiggerShotsEffect) , menuName = "Effects/" +  nameof(BiggerShotsEffect))]
public class BiggerShotsEffect : Effect
{
    public float sizePercentage;
    public float damagePercentage;
    public float massIncrease;
    
    public override void Init(Entity entity)
    {
        base.Init(entity);
        Apply(entity);
    }

    private void Apply(Entity entity)
    {
        var transform1 = entity.transform;
        transform1.localScale *= 1 + stack * sizePercentage;
        
        Shot shot = entity.GetComponent<Shot>();
        shot.GetComponent<Rigidbody2D>().mass = massIncrease * stack;
        if (shot != null)
        {
            shot.Damage = shot.Damage.AddMultiplier(damagePercentage);
        }
    }
}
