﻿
using UnityEngine;

[CreateAssetMenu( fileName = nameof(StunEffect) , menuName = "Effects/" +  nameof(StunEffect))]
public class StunEffect : Effect,IOnStackAddedListener,IOnDurationEndedListener
{
    public override void Init(Entity entity)
    {
        base.Init(entity);
        if(entity.Movement != null)
            entity.Movement.IsOffline = true;
        if(entity.WeaponsSystem != null)
            entity.WeaponsSystem.IsOffline = true;
        if (entity.Brain != null)
            entity.Brain.IsOffline = true;
    }

    public void OnStackAdded(Entity entity)
    {
        ResetActiveTime();
    }

    public void OnDurationEnded(Entity entity)
    {
        if(entity.Movement != null)
            entity.Movement.IsOffline = false;
        if(entity.WeaponsSystem != null)
            entity.WeaponsSystem.IsOffline = false;
        if (entity.Brain != null)
            entity.Brain.IsOffline = false;
    }
}
