﻿using UnityEngine;

[CreateAssetMenu( fileName = nameof(BouncingEffect) , menuName = "Effects/" +  nameof(BouncingEffect))]
public class BouncingEffect : Effect,IOnHitListener
{
    public override void Init(Entity entity)
    {
        base.Init(entity);
        if (entity is Shot shot)
        {
            shot.PreventDestruction(this);
            shot.Range = shot.Range.AddMultiplier(.3f * stack);
        }
    }
    
    public void OnHit(Entity entity,Collision2D collision)
    {
        if(stack < 0 || !(entity is Shot shot))
            return;
        stack--;
        if (stack < 0)
        {
            shot.AllowDestruction(this);
        }
        else
        {
            // Bounce
            Vector2 inNormal = collision.contacts[0].normal;
            shot.Direction = Vector2.Reflect(shot.Direction, inNormal);   
        }
    }
}
