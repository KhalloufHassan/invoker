﻿using UnityEngine;

[CreateAssetMenu( fileName = nameof(SplitShotEffect) , menuName = "Effects/" +  nameof(SplitShotEffect))]
public class SplitShotEffect : Effect,IOnHitListener
{

    public void OnHit(Entity entity,Collision2D collision)
    {
        if (stack > 0)
        {
            stack--;
            for (int i = 0; i < 2; i++)
            {
                Shot newSplit = Instantiate(entity.gameObject).GetComponent<Shot>();
                newSplit.Direction = entity.GetComponent<Shot>().Direction.Rotate(90 * (i % 2 == 0 ? -1 : 1));
                newSplit.Movement.MovementSpeed = entity.Movement.MovementSpeed;
            }
        }
    }
}