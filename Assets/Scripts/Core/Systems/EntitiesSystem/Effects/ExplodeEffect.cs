﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu( fileName = nameof(ExplodeEffect) , menuName = "Effects/" +  nameof(ExplodeEffect))]
public class ExplodeEffect : Effect,IOnDeathListener
{
    public float AoeRadius;
    public FloatStat Damage;
    
    private Collider2D[] colliders = new Collider2D[10]; // to avoid garbage collection

    public void OnDeath(Entity entity)  => Explode(entity);

    private void Explode(Entity entity)
    {
        colliders.Clear();
        Physics2D.OverlapCircleNonAlloc(entity.gameObject.transform.position, AoeRadius, colliders);
        foreach (var go in colliders.Select(c => c == null ? null : c.gameObject))
        {
            if (go == null || go == entity.gameObject)
                continue;
            go.GetComponent<Damagable>()?.TakeDamage(Damage * stack * intensity);
        }
    }
}
