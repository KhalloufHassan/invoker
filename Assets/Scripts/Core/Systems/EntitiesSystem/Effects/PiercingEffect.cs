﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu( fileName = nameof(PiercingEffect) , menuName = "Effects/" +  nameof(PiercingEffect))]
public class PiercingEffect : Effect,IOnHitListener
{
    
    public override void Init( Entity entity)
    {
        base.Init(entity);
        if(entity is Shot shot)
            shot.PreventDestruction(this);
    }

    public void OnHit(Entity entity, Collision2D collision)
    {
        if(stack < 0 || !(entity is Shot shot))
            return;
        stack--;
        if (stack < 0)
        {
            shot.AllowDestruction(this);
        }
        else
        {
            entity.StartCoroutine(Pierce(entity.GetComponent<Collider2D>(),collision.collider,shot));
        }
    }

    private static IEnumerator Pierce(Collider2D c1,Collider2D c2,Shot shot)
    {
        Physics2D.IgnoreCollision(c1,c2);
        yield return new WaitForEndOfFrame();
        shot.Damage = shot.Damage.AddMultiplier(-.3f);
        // yield return new WaitUntil(() => !c1.IsTouching(c2));
        // yield return new WaitForSeconds(.1f);
        // if(c1 != null && c2 != null)
        //     Physics2D.IgnoreCollision(c1,c2,false);
    }
}
