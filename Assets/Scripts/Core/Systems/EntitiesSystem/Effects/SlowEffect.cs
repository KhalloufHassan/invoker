﻿using UnityEngine;

[CreateAssetMenu( fileName = nameof(SlowEffect) , menuName = "Effects/" +  nameof(SlowEffect))]
public class SlowEffect : Effect,IOnDurationEndedListener,IOnStackAddedListener
{
    public float slowPower;
    public override void Init( Entity entity)
    {
        base.Init(entity);
        ApplySlow(entity,stack);
    }

    public void OnDurationEnded(Entity entity)
    {
        if(entity.Movement != null)
            entity.Movement.MovementSpeed += slowPower * intensity * stack;
    }

    public void OnStackAdded(Entity entity)
    {
        ApplySlow(entity,1);
        ResetActiveTime();
    }

    private void ApplySlow(Entity entity,int s)
    {
        if(entity.Movement != null)
            entity.Movement.MovementSpeed -= slowPower * intensity * s;
    }
}
