﻿using System.Collections.Generic;
using UnityEngine;

public class EffectsCaster : MonoBehaviour
{
    public int queueSize = 4;
    public List<Effect> OwnedEffects = new List<Effect>();
    public LinkedList<Effect> CurrentEffects { get; } = new LinkedList<Effect>();


    public void PushEffect(int slot)
    {
        if (OwnedEffects.Count - 1 >= slot)
        {
            CurrentEffects.AddFirst(OwnedEffects[slot]);
            if (CurrentEffects.Count > queueSize) CurrentEffects.RemoveLast();
        }
    }
}
