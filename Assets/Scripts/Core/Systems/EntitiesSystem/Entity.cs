﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Entity : MonoBehaviour
{
    // Shortcuts for all other systems
    [SerializeField] private Movement m_movement;
    [SerializeField] private EffectsCaster m_effectsCaster;
    [SerializeField] private WeaponsSystem m_weaponsSystem;
    [SerializeField] private Damagable m_damagable;
    [SerializeField] private Brain m_brain;
    [SerializeField] private ItemsSystem m_itemsSystem;

    public Movement Movement => m_movement;
    public EffectsCaster EffectsCaster => m_effectsCaster;
    public WeaponsSystem WeaponsSystem => m_weaponsSystem;
    public Damagable Damagable => m_damagable;
    public Brain Brain => m_brain;
    public ItemsSystem ItemsSystem => m_itemsSystem;

    // The exposed effects for the inspector
    public List<Effect> Effects;

    // list of added effects
    private readonly List<Effect> effects = new List<Effect>();

    private void Start()
    {
        foreach (Effect effect in Effects)
        {
            AddEffect(effect);
        }
    }

    protected virtual void Update()
    {
        foreach (Effect effect in effects)
        {
            if(effect.isHost)
                continue;
            
            // Make sure the effect is initialized
            if (!effect.IsInitialized)
            {
                effect.Init(this);
            }
            
            // Invoke the update
            (effect as IOnUpdateListener)?.OnUpdate(this);

            // Check Duration
            if(effect.DurationEnded)
                (effect as IOnDurationEndedListener)?.OnDurationEnded(this);
        }

        effects.RemoveAll(e => e.DurationEnded && !e.isHost);
    }

    public void AddEffect(Effect effect,bool isHost = false,int stack = 1,float intensity = 1)
    {
        var currentEffect = effects.FirstOrDefault(e => e.Equals(effect));
        if (currentEffect != null)
        {
            currentEffect.stack++;
            if(!isHost)
                (currentEffect as IOnStackAddedListener)?.OnStackAdded(this);
        }
        else
        {
            Effect newEffect = Instantiate(effect);
            newEffect.isHost = isHost;
            newEffect.stack = stack;
            newEffect.intensity = intensity;
            if(!isHost)
                newEffect.Init(this);
            effects.Add(newEffect);
        }
    }
    
    public void RemoveEffect(Effect effect)
    {
        Effects.RemoveAll(e => e.Equals(effect));
    }

    public void DestroyEntity()
    {
        foreach (Effect effect in effects) (effect as IOnDeathListener)?.OnDeath(this);
        StartCoroutine(DestroyNextFrame());
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnHit(collision);
    }

    protected virtual void OnHit(Collision2D collision)
    {
        foreach (var e in effects)
        {
            Entity otherEffectSystem = collision.gameObject.GetComponent<Entity>();
            if(e.isHost && otherEffectSystem != null)
                otherEffectSystem.AddEffect(e);
            (e as IOnHitListener)?.OnHit(this,collision);
        }
    }

    private IEnumerator DestroyNextFrame()
    {
        yield return new WaitForEndOfFrame();
        Destroy(gameObject);
    }
}
