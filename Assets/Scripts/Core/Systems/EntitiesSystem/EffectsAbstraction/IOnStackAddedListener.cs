public interface IOnStackAddedListener
{
        void OnStackAdded(Entity entity);
}