public interface IOnDurationEndedListener
{
    void OnDurationEnded(Entity entity);
}