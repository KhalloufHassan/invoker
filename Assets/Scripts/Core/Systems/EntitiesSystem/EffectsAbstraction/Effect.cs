﻿using System;
using UnityEngine;

public abstract class Effect : ScriptableObject
{
    [Header("Info")]
    public Sprite icon;
    public string effectName;
    
    public FloatStat Duration = float.PositiveInfinity;
    public bool IsNegativeEffect;//TODO remove this
    [HideInInspector] public bool isHost;
    [HideInInspector] public int stack;
    [HideInInspector] public float intensity;

    private DateTime StartTime;
    private TimeSpan LifeTime => DateTime.Now - StartTime;
    public bool DurationEnded => LifeTime.TotalSeconds > Duration.Value;
    public bool IsInitialized { get; private set; }

    public virtual void Init(Entity entity)
    {
        StartTime = DateTime.Now;
        IsInitialized = true;
    }

    protected void ResetActiveTime()
    {
        StartTime = DateTime.Now;
    }

    public override bool Equals(object other)
    {
        if (!(other is Effect otherEffect))
            return false;
        return effectName == otherEffect.effectName;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
