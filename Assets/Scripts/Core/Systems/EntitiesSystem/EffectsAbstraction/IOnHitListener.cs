using UnityEngine;

public interface IOnHitListener
{
    void OnHit(Entity entity,Collision2D collision);
}