﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Systems
{
    public class System : MonoBehaviour
    {
        public bool IsOffline { get; set; }
    }
}
