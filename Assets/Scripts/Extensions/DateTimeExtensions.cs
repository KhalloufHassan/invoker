﻿using System;

public static class DateTimeExtensions
{
    public static bool HasSecondsPassed(this DateTime dateTime, float seconds)
    {
        return (DateTime.Now - dateTime).TotalSeconds > seconds;
    }
}
