﻿using UnityEngine;

public static class VectorExtensions
{
    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
         
        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public static Vector3 AddY(this Vector3 v, float y)
    {
        v.y += y;
        return v;
    }

    public static Vector3 AddX(this Vector3 v, float x)
    {
        v.x += x;
        return v;
    }
}
