﻿using UnityEngine;

public static class Collider2DExtensions
{
   
   public static Vector2 GetHidingPointFromTarget(this Collider2D collider,Transform target,float extraDistance = 0)
   {
      Vector2 diff =  target.position - collider.transform.position;
        
      if(Mathf.Abs(diff.x) > Mathf.Abs(diff.y))
      {
         if (diff.x > 0)
            return collider.GetLeftBound(extraDistance);
         else
            return collider.GetRightBound(extraDistance);
      }
      else
      {
         if(diff.y > 0)
            return collider.GetBottomBound(extraDistance);
         else
            return collider.GetTopBound(extraDistance);
      }
   }
   
   public static Vector2 GetTopBound(this Collider2D collider2D, float extraDistance = 0)
   {
      var bounds = collider2D.bounds;
      return bounds.center.AddY(bounds.extents.y + extraDistance);
   }
   
   public static Vector2 GetBottomBound(this Collider2D collider2D, float extraDistance = 0)
   {
      var bounds = collider2D.bounds;
      return bounds.center.AddY(-bounds.extents.y - extraDistance);
   }
   
   public static Vector2 GetRightBound(this Collider2D collider2D, float extraDistance = 0)
   {
      var bounds = collider2D.bounds;
      return bounds.center.AddX(bounds.extents.x + extraDistance);
   }
   
   public static Vector2 GetLeftBound(this Collider2D collider2D, float extraDistance = 0)
   {
      var bounds = collider2D.bounds;
      return bounds.center.AddX(-bounds.extents.x - extraDistance);
   }
}
