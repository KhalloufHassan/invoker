﻿using System.Collections.Generic;
using static UnityEngine.Random;

public static class CollectionsExtensions
{
    public static void Clear<T>(this T[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = default;
        }
    }

    public static T Random<T>(this IList<T> list) => list.Count == 0 ? default : list[Range(0, list.Count)];
}
