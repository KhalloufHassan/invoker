﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AreasMapGenerator))]
public class MapGeneratorEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Regenerate"))
        {
            AreasMapGenerator generator = (AreasMapGenerator) target;
            generator.Regenerate();
        }
    }
}
