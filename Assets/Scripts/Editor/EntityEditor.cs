﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Entity))]
public class EntityEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Assign all components"))
        {
            Entity entity = (Entity) target;
            serializedObject.FindProperty("m_movement").objectReferenceValue = entity.GetComponent<Movement>();
            serializedObject.FindProperty("m_effectsCaster").objectReferenceValue = entity.GetComponent<EffectsCaster>();
            serializedObject.FindProperty("m_weaponsSystem").objectReferenceValue = entity.GetComponent<WeaponsSystem>();
            serializedObject.FindProperty("m_damagable").objectReferenceValue = entity.GetComponent<Damagable>();
            serializedObject.FindProperty("m_brain").objectReferenceValue = entity.GetComponent<Brain>();
            serializedObject.FindProperty("m_itemsSystem").objectReferenceValue = entity.GetComponent<ItemsSystem>();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
