using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(DungeonGenerator))]
    public class DungeonGeneratorInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            DungeonGenerator generator = (DungeonGenerator) target;

            if (GUILayout.Button("Regenerate"))
            {
                generator.Generate();
            }
        }
    }
}