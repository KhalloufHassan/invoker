﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Door))]
public class DoorCustomEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Lock"))
        {
            Door door = (Door) target;
            door.Lock();
        }
        
        if (GUILayout.Button("Unlock"))
        {
            Door door = (Door) target;
            door.Unlock();
        }
    }
}
