﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomWeaponMod : MonoBehaviour
{
    public List<WeaponMod> Effects;
    public TextMesh text;

    private WeaponMod mod;

    private void Start()
    {
        WeaponMod m = Effects.Random();
        mod = m;
        text.text = m.modName;
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Invoker")
        {
            collision.gameObject.GetComponent<WeaponsSystem>().AttachMod(mod);
            Destroy(gameObject);
        }
    }
}
