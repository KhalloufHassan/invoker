﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WeaponItem : MonoBehaviour
{
    public Weapon weapon;
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Invoker")
        {
            collision.gameObject.GetComponent<WeaponsSystem>().AddWeapon(weapon);
            Destroy(gameObject);
        }
    }
}
