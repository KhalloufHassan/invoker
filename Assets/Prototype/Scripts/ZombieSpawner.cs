﻿using System;
using Boo.Lang;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject zomniePrefab;
    public Transform spawnPosition;
    public float timeToSpawn;
    public int maxConcurrentSpawns;

    private float timeLeft;
    private List<GameObject> spawns;

    private void Start()
    {
        timeLeft = timeToSpawn;
        spawns = new List<GameObject>();
    }

    private void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0 && CountSpawns() < maxConcurrentSpawns)
        {
            spawns.Add(Instantiate(zomniePrefab,spawnPosition.position,Quaternion.identity));
            timeLeft = timeToSpawn;
        }
    }

    private int CountSpawns()
    {
        spawns.RemoveAll(s => s == null);
        return spawns.Count;
    }
}
