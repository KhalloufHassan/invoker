﻿using System.Collections.Generic;
using UnityEngine;

public class FireSurface : MonoBehaviour
{
    private List<Entity> drowningEntities;
    public float damagePerSecond = 20; 

    private void Start()
    {
        drowningEntities = new List<Entity>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Entity entity = other.GetComponent<Entity>();
        if (entity != null && entity.Damagable != null)
        {
            drowningEntities.Add(entity);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Entity entity = other.GetComponent<Entity>();
        if (entity != null && entity.Damagable != null)
        {
            drowningEntities.Remove(entity);
        }
    }

    private void Update()
    {
        foreach (Entity entity in drowningEntities)
        {
            entity.Damagable.TakeDamage(Time.deltaTime * damagePerSecond);
        }
    }
}
