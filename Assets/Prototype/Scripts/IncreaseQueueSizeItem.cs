﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseQueueSizeItem : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Invoker")
        {
            collision.gameObject.GetComponent<EffectsCaster>().queueSize++;
            Destroy(gameObject);
        }
    }
}
