﻿using UnityEngine;

public class TimeWrap : MonoBehaviour
{
    public float timeScale = 1;

    private void Update()
    {
        Time.timeScale = timeScale;
    }
}
