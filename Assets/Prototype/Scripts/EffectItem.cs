﻿using UnityEngine;
using UnityEngine.Serialization;

public class EffectItem : MonoBehaviour
{
    [FormerlySerializedAs("effect")] public Effect effectController;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Invoker")
        {
            collision.gameObject.GetComponent<EffectsCaster>().OwnedEffects.Add(effectController);
            Destroy(gameObject);
        }
    }
}
