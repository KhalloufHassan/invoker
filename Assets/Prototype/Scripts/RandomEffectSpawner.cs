﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(EffectItem))]
public class RandomEffectSpawner : MonoBehaviour
{
    public List<Effect> Effects;
    public TextMesh text;

    private void Start()
    {
        Effect e = Effects[Random.Range(0, Effects.Count - 1)];
        GetComponent<EffectItem>().effectController = e;
        GetComponent<SpriteRenderer>().sprite = e.icon;
        text.text = e.effectName;
    }
}
