﻿using System;
using UnityEngine;

public class Door : MonoBehaviour
{
    public GameObject lockedSprite;
    public GameObject unlockedSprite;
    public Animator animator;
    
    public void SetLocked(bool locked)
    {
        lockedSprite.SetActive(locked);
        unlockedSprite.SetActive(!locked);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Open();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Close();
    }

    [ContextMenu("Open")]
    public void Open()
    {
        if(lockedSprite.activeInHierarchy)
            return;
        animator.Play("DoorUnlock");
    }
    
    [ContextMenu("Close")]
    public void Close()
    {
        if(lockedSprite.activeInHierarchy)
            return;
        animator.Play("DoorLock");
    }


    [ContextMenu("Lock")]
    public void Lock()
    {
        SetLocked(true);
    }

    [ContextMenu("Unlock")]
    public void Unlock()
    {
        SetLocked(false);
    }
}
