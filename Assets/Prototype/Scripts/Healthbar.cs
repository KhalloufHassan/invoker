﻿using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Slider slider;
    public Damagable Damagable;

    private void Update()
    {
        slider.maxValue = Damagable.TotalHP;
        slider.value = Damagable.HP;
    }
}
