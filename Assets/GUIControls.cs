﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GUIControls : MonoBehaviour
{
    public Image panel;
    public Image invokePrefab;
    public Slider HPSlider;

    public EffectsCaster effectsCaster;
    public Damagable damagable;

    private Image[] invokes;
    private int lastQueueSize;

    private void Start()
    {
        HPSlider.maxValue = damagable.TotalHP;
    }

    private void Update()
    {
        if (lastQueueSize != effectsCaster.queueSize)
            ReConstructQueueUI();
        for (int i = 0; i < effectsCaster.queueSize; i++)
        {
            Effect element = effectsCaster.CurrentEffects.Count > i ? effectsCaster.CurrentEffects.ElementAt(i) : null;
            invokes[i].sprite = element == null ? null : element.icon;
        }

        HPSlider.value = damagable != null ? damagable.HP : 0;
    }

    private void ReConstructQueueUI()
    {
        if(invokes != null && invokes.Length > 0)
            foreach (Image invoke in invokes)
            {
                Destroy(invoke.gameObject);
            }
        int invokeSize = effectsCaster.queueSize;
        invokes = new Image[invokeSize];
        for (int i = 0; i < invokeSize; i++)
        {
            invokes[i] = Instantiate(invokePrefab);
            invokes[i].transform.SetParent(panel.transform,false);
        }
        var rect = panel.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(invokeSize * 110, rect.sizeDelta.y);
        lastQueueSize = effectsCaster.queueSize;
    }
}
