﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public GameReferences gameReferences;

    private void Update()
    {
        if (gameReferences != null && gameReferences.Player != null)
        {
            Vector3 position = gameReferences.Player.transform.position;
            gameObject.transform.position = new Vector3(position.x, position.y, transform.position.z);
        }
    }
}
