﻿using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputController : MonoBehaviour
{

    [SerializeField] private GameReferences gameReferences;

    [SerializeField] private InputActionReference moveAction;
    [SerializeField] private InputActionReference fireAction;
    [SerializeField] private InputActionReference lookAction;

    [SerializeField] private InputActionReference invoke1Action;
    [SerializeField] private InputActionReference invoke2Action;
    [SerializeField] private InputActionReference invoke3Action;
    [SerializeField] private InputActionReference invoke4Action;
    [SerializeField] private InputActionReference invoke5Action;

    private void Update()
    {
        if(gameReferences == null || gameReferences.Player == null)
            return;
        Entity entity = gameReferences.PlayerEntity;
        int v = 0;
        int h = 0;
        if (Input.GetKey(KeyCode.W)) v = 1;
        else if (Input.GetKey(KeyCode.S)) v = -1;
        
        if (Input.GetKey(KeyCode.D)) h = 1;
        else if (Input.GetKey(KeyCode.A)) h = -1;
        
        entity.Movement.MoveInDirection(new Vector2(h,v));

        // convert mouse position into world coordinates
        Vector3 mouseScreenPosition = Camera.main.ScreenToWorldPoint(lookAction.action.ReadValue<Vector2>());
        entity.Movement.LookAt(mouseScreenPosition);

        if(Input.GetKey(KeyCode.LeftShift))
            entity.Movement.TakeCover();
        if(Input.GetKeyUp(KeyCode.LeftShift))
            entity.Movement.LeaveCover();
        
        if (Input.GetKeyDown(KeyCode.Mouse0)) 
            entity.WeaponsSystem.Fire(entity.Movement.VisionDirection);
        else
            entity.WeaponsSystem.Reload();

        if (Input.GetKeyDown(KeyCode.Alpha1) )
            entity.EffectsCaster.PushEffect(0);

        if (Input.GetKeyDown(KeyCode.Alpha2) )
            entity.EffectsCaster.PushEffect(1);

        if (Input.GetKeyDown(KeyCode.Alpha3) )
            entity.EffectsCaster.PushEffect(2);

        if (Input.GetKeyDown(KeyCode.Alpha4) )
            entity.EffectsCaster.PushEffect(3);
        
        if (Input.GetKeyDown(KeyCode.Alpha5) )
            entity.EffectsCaster.PushEffect(4);
    }
}
