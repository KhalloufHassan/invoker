﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prototype : MonoBehaviour
{
    public GameObject player;
    public GameObject choicePanel;
    
    public void StartWithController()
    {
        Destroy(choicePanel);
    }

    public void StartWithMouseAndKeyboard()
    {
        player.GetComponent<PlayerInputController>().enabled = true;
        Destroy(choicePanel);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
